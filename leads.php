<?php
/**
 * Created by PhpStorm.
 * User: justin
 * Date: 2/12/16
 * Time: 12:42 PM
 */
$page_title = 'Hot Leads';
require_once 'db-config.php';
require_once 'functions.php';
require_once 'header.php';

$pageNum = isset($_GET['p']) ? $_GET['p'] : 1;
$mailed = isset($_GET['hidemailed']) && $_GET['hidemailed'] == 1 ? $_GET['hidemailed'] : null;
$willuse = isset($_GET['willuse']) && $_GET['willuse'] == 1 ? $_GET['willuse'] : null;
$horseshoe = (isset($_GET['horseshoe']) && $_GET['horseshoe'] == 1) ? "AND mailing_address NOT LIKE '%HORSESHOE BAY%' " : '';
$po_box = (isset($_GET['po_box']) && $_GET['po_box'] == 1) ? "AND mailing_address NOT LIKE '%PO BOX%' " : '';
$sql = $horseshoe . $po_box;

$offset = ($pageNum - 1) * 50;
try {
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    if ($mailed !== null && $willuse !== null) {
        $totalResults = $db->query("SELECT COUNT(*) FROM properties WHERE mailed=0 ".$sql."AND will_not_use=0")->fetchColumn();
    } elseif ($mailed !== null) {
        $totalResults = $db->query("SELECT COUNT(*) FROM properties WHERE mailed=0 ".$sql)->fetchColumn();
    } elseif ($willuse !== null) {
        $totalResults = $db->query("SELECT COUNT(*) FROM properties WHERE will_not_use=0 ".$sql)->fetchColumn();
    } else {
        $totalResults = $db->query("SELECT COUNT(*) FROM properties WHERE 1 ".$sql)->fetchColumn();
    }
} catch(PDOException $e) {
    echo "Error: " . $e->getMessage();
}
$totalResults = isset($totalResults) ? $totalResults : 0;
if( $totalResults % 50 ) {
    //add an extra page if there is a remainder
    $totalPages = intval($totalResults / 50 + 1);
} else {
    $totalPages = intval($totalResults / 50);
}

?>

<?php paginate_pages($totalPages, $pageNum); ?>
<div>
    <a href="#" onclick="UpdateQueryString('hidemailed','<?php echo $mailed == 1 ? '0' : '1'; ?>')">Toggle mailed properties</a><br>
    <a href="#" onclick="UpdateQueryString('willuse','<?php echo $willuse == 1 ? '0' : '1'; ?>')">Toggle "won't use" properties</a><br>
    <a href="#" onclick="UpdateQueryString('horseshoe','<?php echo $horseshoe !== '' ? '0' : '1'; ?>')">Toggle "Horseshoe bay" properties</a><br>
    <a href="#" onclick="UpdateQueryString('po_box','<?php echo $po_box !== '' ? '0' : '1'; ?>')">Toggle "PO BOX" properties</a>
</div>
<h3>Page <?php echo $pageNum; ?></h3>
<table cellspacing="0" style='border: solid 1px black;'>
    <?php
        echo "<tr><th>Status</th><th>Property ID</th><th>Owner Name</th><th>Mailing Address</th><th>Property Address</th><th>Legal Description</th><th>Property Type <a href='http://www.taxnetusa.com/research/texas/sptb.php' target='_blank'><sup>?</sup></a></th><th>Neighborhood CD</th></tr>";

        try {
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            if ($mailed !== null && $willuse !== null) {
                $stmt = $db->prepare("SELECT mailed, will_not_use, property_id, owner_name, mailing_address, address, legal_description, property_land, neighborhood_cd FROM properties WHERE mailed=0 ".$sql."AND will_not_use=0 ORDER BY property_id DESC LIMIT :limit, 50");
            } elseif ($mailed !== null) {
                $stmt = $db->prepare("SELECT mailed, will_not_use, property_id, owner_name, mailing_address, address, legal_description, property_land, neighborhood_cd FROM properties WHERE mailed=0 ".$sql."ORDER BY property_id DESC LIMIT :limit, 50");
            } elseif ($willuse !== null) {
                $stmt = $db->prepare("SELECT mailed, will_not_use, property_id, owner_name, mailing_address, address, legal_description, property_land, neighborhood_cd FROM properties WHERE will_not_use=0 ".$sql."ORDER BY property_id DESC LIMIT :limit, 50");
            } else {
                $stmt = $db->prepare("SELECT mailed, will_not_use, property_id, owner_name, mailing_address, address, legal_description, property_land, neighborhood_cd FROM properties WHERE 1 ".$sql."ORDER BY property_id DESC LIMIT :limit, 50");
            }
            $exprt = $db->prepare("SELECT mailed, will_not_use, property_id, owner_name, mailing_address, address, legal_description, property_land, neighborhood_cd FROM properties WHERE 1 ".$sql."ORDER BY property_id DESC");
            $stmt->bindParam(':limit',$offset, PDO::PARAM_INT);

            $stmt->execute();
            $exprt->execute();


            // set the resulting array to associative
            $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
            $export_result = $exprt->setFetchMode(PDO::FETCH_ASSOC);
//            export_to_spreadsheet($exprt);
            foreach(new TableRows(new RecursiveArrayIterator($stmt->fetchAll())) as $k=>$v) {
                echo nl2br($v);
            }

        } catch(PDOException $e) {
            echo "Error: " . $e->getMessage();
        }
    ?>
</table>

<span id="results"></span>
<div id="#progress-bar"></div>
<a href="export.php?<?php echo $_SERVER['QUERY_STRING']; ?>">export all rows with current parameters</a>
<?php paginate_pages($totalPages, 'leads.php');
require_once 'footer.php';