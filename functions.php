<?php
/**
 * Created by PhpStorm.
 * User: justin
 * Date: 2/12/16
 * Time: 2:54 PM
 */


/*
 * TABLE ROWS ITERATOR
 */
class TableRows extends RecursiveIteratorIterator {

    function __construct(RecursiveArrayIterator $it) {
        parent::__construct($it, self::LEAVES_ONLY);
    }

    function current() {
        $checked = parent::current() == 1 ? 'checked' : '';
        if (parent::key() === 'mailed') {
            return "<td class='property-update'><form action='property-table.php' method='post'><label for='mailed'>Mailed</label><input type='checkbox' class='property-mailed' name='mailed' ".$checked." value='". parent::current() ."'>";
        } elseif (parent::key() === 'will_not_use') {
            return "<br><label for='will-not-use'>Won't Use</label><input type='checkbox' class='property-wont-use' name='will-not-use' ".$checked." value='". parent::current() ."'><input type='hidden' name='property-id' value=''></form></td>";
        }
        return "<td class='". parent::key() . "'>" . parent::current(). "</td>";
    }

    function beginChildren() {
        echo "<tr>";
    }

    function endChildren() {
        echo "</tr>" . "\n";
    }
}

/*
 * PAGINATION
 */

function paginate_pages($totalPages, $current_page){
    ?>
    <ul class="pagination">
        <?php
            for( $i = 1; $i <= $totalPages; $i++ ) {
                if ( $current_page == $i ) {
                    echo "<li>$i</li>";
                } else {
                    echo "<li><a href='#' onclick=\"UpdateQueryString('p','$i')\">$i</a></li>";
                }
            }
        ?>
    </ul>
<?php
}

function export_to_spreadsheet($result){
    // Clear any previous output
    ob_end_clean();
// I assume you already have your $result
    $num_fields = $result->columnCount();

// Fetch MySQL result headers
    $headers = array();
    $headers[] = "[Row]";
    for ($i = 0; $i < $num_fields; $i++) {
        $headers[] = strtoupper($result->getColumnMeta($i)['name']);
    }

// Filename with current date
    $current_date = date("y/m/d");
    $filename = "CadSearchExport" . $current_date . ".csv";

// Open php output stream and write headers
    $fp = fopen('php://output', 'w');
    if ($fp && $result) {
        header('Content-Type: text/csv');
        header('Content-Disposition: attachment; filename='.$filename);
        header('Pragma: no-cache');
        header('Expires: 0');
//        echo "Cad Search Export\n";
        // Write mysql headers to csv
        fputcsv($fp, $headers);
        $row_tally = 0;
        // Write mysql rows to csv
        while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
            $row_tally = $row_tally + 1;
            echo $row_tally.",";
            fputcsv($fp, array_values($row));
        }
        fclose($fp);
        die;
    }
}