<?php
/**
 * Created by PhpStorm.
 * User: justin
 * Date: 2/11/16
 * Time: 10:28 PM
 */

//keyword search
$propertyUrl = $_GET['url'];


// create curl resource
$ch = curl_init();

// set url
curl_setopt($ch, CURLOPT_URL, $propertyUrl);

//return the transfer as a string
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

// $output contains the output string
$output = curl_exec($ch);

// close curl resource to free up system resources
curl_close($ch);

echo $output;