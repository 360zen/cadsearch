<?php
/**
 * Created by PhpStorm.
 * User: justin
 * Date: 2/23/16
 * Time: 3:23 AM
 */
$page_title = 'Export Database';

require_once 'db-config.php';
require_once 'functions.php';
require_once 'header.php';

$pageNum = isset($_GET['p']) ? $_GET['p'] : 1;
$mailed = isset($_GET['hidemailed']) && $_GET['hidemailed'] == 1 ? $_GET['hidemailed'] : null;
$willuse = isset($_GET['willuse']) && $_GET['willuse'] == 1 ? $_GET['willuse'] : null;
$horseshoe = (isset($_GET['horseshoe']) && $_GET['horseshoe'] == 1) ? "AND mailing_address NOT LIKE '%HORSESHOE BAY%' " : '';
$po_box = (isset($_GET['po_box']) && $_GET['po_box'] == 1) ? "AND mailing_address NOT LIKE '%PO BOX%' " : '';
$sql = $horseshoe . $po_box;
$offset = ($pageNum - 1) * 50;

try {
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    if ($mailed !== null && $willuse !== null) {
        $exprt = $db->prepare("SELECT mailed, will_not_use, property_id, owner_name, mailing_address, address, property_land, neighborhood_cd FROM properties WHERE mailed=0 ".$sql."AND will_not_use=0 ORDER BY property_id DESC");
    } elseif ($mailed !== null) {
        $exprt = $db->prepare("SELECT mailed, will_not_use, property_id, owner_name, mailing_address, address, property_land, neighborhood_cd FROM properties WHERE mailed=0 ".$sql."ORDER BY property_id DESC");
    } elseif ($willuse !== null) {
        $exprt = $db->prepare("SELECT mailed, will_not_use, property_id, owner_name, mailing_address, address, property_land, neighborhood_cd FROM properties WHERE will_not_use=0 ".$sql."ORDER BY property_id DESC");
    } else {
        $exprt = $db->prepare("SELECT mailed, will_not_use, property_id, owner_name, mailing_address, address, property_land, neighborhood_cd FROM properties WHERE 1 ".$sql."ORDER BY property_id DESC");
    }

    $exprt->execute();

    export_to_spreadsheet($exprt);


} catch(PDOException $e) {
    echo "Error: " . $e->getMessage();
}