<?php
/**
 * Created by PhpStorm.
 * User: justin
 * Date: 2/11/16
 * Time: 11:11 PM
 */
require_once 'db-config.php';

$id = urldecode($_GET['id']);
$legalDescription = urldecode($_GET['legalDescription']);
$geoID = urldecode($_GET['geoID']);
$agentCode = urldecode($_GET['agentCode']);
$type = urldecode($_GET['type']);

//location
$address = urldecode($_GET['address']);
$mapID = urldecode($_GET['mapID']);
$neighborhoodCD = urldecode($_GET['neighborhoodCD']);
$subdivision = urldecode($_GET['subdivision']);

//owner
$ownerID = urldecode($_GET['ownerID']);
$ownerName = urldecode($_GET['ownerName']);
$mailingAddress = urldecode($_GET['mailingAddress']);
$percentOwnership = urldecode($_GET['percentOwnership']);
$exemptions = urldecode($_GET['exemptions']);

$propertyType = urldecode($_GET['land']);
$propertyTypeCategory = substr($propertyType, 0, 1);
$propertyLand = substr($propertyType, 0, 2);
var_dump($propertyType,$propertyTypeCategory);

if ($mailingAddress !== $address && (substr($mailingAddress, 0, 5) !== substr($address, 0, 5)) && (($propertyTypeCategory === 'A') || ($propertyTypeCategory === 'B') || ($propertyTypeCategory == false)) ) {
    try {
        $stmt = $db->prepare("INSERT INTO properties (property_id, legal_description, geo_id, agent_code, property_type, property_land, address, subdivision, map_id, neighborhood_cd, owner_id, owner_name, mailing_address, percent_ownership, exemptions)
                              VALUES (:property_id, :legal_description, :geo_id, :agent_code, :property_type, :property_land, :address, :subdivision, :map_id, :neighborhood_cd, :owner_id, :owner_name, :mailing_address, :percent_ownership, :exemptions)
                              ON DUPLICATE KEY UPDATE property_id= :property_id, legal_description= :legal_description, geo_id= :geo_id, agent_code= :agent_code, property_type= :property_type, property_land= :property_land, address= :address, subdivision= :subdivision, map_id= :map_id, neighborhood_cd= :neighborhood_cd, owner_id= :owner_id, owner_name= :owner_name, mailing_address= :mailing_address, percent_ownership= :percent_ownership, exemptions= :exemptions");

        $stmt->execute( array(
            'property_id' => $id,
            'legal_description' => $legalDescription,
            'geo_id' => $geoID,
            'agent_code' => $agentCode,
            'property_type' => $type,
            'property_land' => $propertyLand,
            'address' => $address,
            'subdivision' => $subdivision,
            'map_id' => $mapID,
            'neighborhood_cd' => $neighborhoodCD,
            'owner_id' => $ownerID,
            'owner_name' => $ownerName,
            'mailing_address' => $mailingAddress,
            'percent_ownership' => $percentOwnership,
            'exemptions' => $exemptions
        ));
        if (!$stmt) {
            echo "\nPDO::errorInfo():\n";
            print_r($db->errorInfo());
        }
        echo 'it worked';
    } catch(PDOException $ex) {
        echo 'Error connecting to the database';
        echo $ex->getMessage();
    }
}
