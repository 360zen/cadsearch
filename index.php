<?php
$page_title = "Property Search";
require_once 'header.php';
?>
<form id="cad-form">
    <h2>Enter criteria</h2>
    <label>Enter <span class="sub-or">Keyword</span><span class="sub-or off">Subdivision ID, Name</span>:</label>
    <input type="text" name="search" id="search"> <span class="sub-or off">OR</span>
    <select name="subdivisions-dropdown" id="subdivisions-dropdown">
        <option value="none">SELECT SUBDIVISION</option>
    </select>
    <br>
    <label for="search-type">Search Type:</label>
    <input type="radio" name="search-type" value="text" checked>Text
    <input type="radio" name="search-type" value="subdivision">Subdivision (by ID#)<br>
    <input type="submit" name="cad-form-submit" id="cad-form-submit" class="button" value="Submit">
</form>
<div id="waiting-text">
    <p>Processing. Please be patient and allow the query to finish. Refresh the page to begin a new search.</p>
    <p id="total">Total results: <span id="total-results"></span></p>
    <p id="remaining"><span id="results-remaining"></span> results remaining.</p>
    <p id="time">Approximately <span id="time-remaining"></span> minutes remaining.</p>
    <div id="percentage-bar">
        <div id="percent-completed"><span id="percentage-text"></span></div>
    </div>
    <div>
        <p id="results"></p>
    </div>
    <a href="#" id="reset" class="button">Stop/Reset</a>
    <div id="#progress-bar"></div>
</div>
<p class="note"><em>Enter a keyword, and click submit. Please be patient and leave the tab/window open. It may take a while (2 seconds for every property found).</em></p>
<?php
require_once 'footer.php';