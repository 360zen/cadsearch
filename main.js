/**
 * Created by justin on 2/11/16.
 */
var subdivisions = ["10025 - A  D ACRES","10030 - AIRVIEW SUBDIVISION","10033 - ANGELINA COVE","10035 - ARROWHEAD ESTATES","10050 - BARNETT HEIRS","10055 - BAY COUNTRY","10060 - BEACH POINT","10075 - BIG OAKS","10085 - BLACK ROCK ESTATES","10100 - BLUEBONNET HILLS","10125 - BLUE LAKE ESTATES","10140 - BONNY COVE","10150 - BOXWOOD","10160 - BRIDGEPOINT","10165 - BROOKLYNN VILLAGE","10175 - BUCHANAN LAKE VILLAGE","10180 - BUSINESS CENTER SUBDIVISION","10190 - CARAWAY 1","10200 - CARLSON","10225 - CASTELL","10226 - CASTELL RIVER RANCH ON THE LLANO","10228 - HILL COUNTRY COVE","10230 - CHAPARRAL ESTATES","10245 - COMANCHE COVE SUBDIVISION","10250 - COMANCHE RANCHETTES","10275 - COMANCHE RANCHERIAS","10280 - CRABILL SUBSIVISION","10300 - CRESTWOOD","10308 - CYPRESS COVE AT WATKINS POINT","10315 - DEER COUNTRY","10325 - DEER HILLS","10350 - DEER HAVEN","10375 - DELTA ACRES","10385 - DIAMOND CROSS","10387 - DIAMOND S RANCH","10395 - DOMINION AT APPLEHEAD ISLAND","10405 - ELLISON","10415 - ELM LODGE","10417 - ENCHANTED VISTAS","10420 - ENCINO COVE","10422 - ESCONDIDO","10425 - EVERETT ACRES","10435 - KINGSLAND FAIRVIEW","10442 - FLAG CREEK OAKS","10443 - FLAG CREEK RANCH","10444 - FLATROCK NORTHEAST","10445 - FLOYD ACRES","10455 - FRED WOOD","10458 - FRENCH JOHN CREEK","10465 - JA GABBART","10475 - GEOLA ESTATES","10477 - GILBERT ACRES","10480 - GLENN COVE POINT SUBD","10485 - GOLDEN BEACH","10495 - GOLF VIEW","10505 - GRAN SABANA","10515 - GRANITE","10520 - GRAND BANK OF TEXAS","10525 - GRANITE COVE","10530 - GRANITE LAKE DEVELOPMENT","10532 - GRANITE RIDGE AT PACKSADDLE","10533 - GRANITE ROCK RV","10535 - GRANITE SHOALS CABIN SITES","10545 - GRANITE SHOALS LAKE ESTATE","10555 - GRANITE SHOALS LAKE SHORES","10565 - GREENWOOD ACRES","10568 - HALLMARK ISLAND","10569 - HICKORY CREEK RANCH","10575 - HIGHLAND PARK","10580 - HIGHLAND TERRACE","10585 - HI LINE POINT","10595 - HILL ACRES","10600 - HILLTOP AT BLUE LAKE","10605 - HILL VIEW ESTATES","10615 - HILL DALE RANCHETTES","10625 - HOFFMAN","10630 - HOUSE MOUNTAIN RANCH","10650 - HORSESHOE BAY PROPER","10675 - HORSESHOE BAY NORTH","10725 - HORSESHOE BAY WEST","10730 - APPLEHEAD ISLAND","10732 - APPLEHEAD WEST","10740 - BAY CLIFF CONDO","10745 - BAY COURT CONDO","10750 - BAY HILL DECK","10775 - BAYSIDE","10800 - BAY VIEW","10825 - BAY VIEW 3","10850 - BAY VIEW VILLAS","10860 - BAY WEST","10865 - BAY WEST OFFICE PARK","10875 - BRANTFORD TOWNHOMES","10885 - CAMPAGNA BELLO CONDO","10900 - CAPE SHORES","10920 - CAPE TERRACE","10925 - CAPE TOWN","10940 - CAPE VILLAS","10950 - CAPE WEST","10975 - CENTER COURT","11000 - COURTLAND","11025 - COURTSIDE","11050 - COURTSIDE VILLA","11075 - COVE WEST","11100 - CREEKHILL NORTH","11130 - CROSSING","11150 - DEER CREEK VILLA","11151 - DIAGONAL CONDO","11152 - EL JARDIN TOWNHOMES","11155 - EMERALD POINT","11175 - ESCAPE BAY POINT","11200 - ESCAPE WEST","11225 - FAIRWAY MANOR","11230 - FAULT LINE COVE GARDEN HOMES CONDOS","11240 - GAUMAN FAMILY DIVISION","11250 - GAZEBO","11255 - GLEN COVE BAY","11260 - GOLF HILLS","11275 - GOLF RIDGE","11300 - GOLF VILLA","11325 - GOOSE POINT","11350 - HACIENDA","11375 - HALE MAKANI","11400 - HARBOR TOWN","11420 - HERON COVE","11450 - HI SKY","11460 - HIGHLAND COVE","11475 - HIGH VISTA","11500 - HILLCREST GARDEN","11525 - HILLSIDE","11530 - HORSESHOE BAY HIGHLANDS","11550 - INLET COVE TOWNHOUSE","11575 - INTERNATIONAL","11610 - ISLAND POINT","11620 - ISLAND VILLAGE","11625 - ISLAND WAY","11626 - LACHITE-GOLF","11627 - LAKESHORE","11650 - LANDING","11675 - LANDMARK 1","11700 - LAS ENCINAS","11710 - LAKE LBJ VILLAGE","11725 - LIGHTHOUSE","11750 - LOMA ATLAS","11752 - LONE OAK CONDO","11755 - LOST ECHO BAY","11775 - LAS PATIOS","11777 - LOS PALOS","11810 - MATERN ISLAND","11815 - 18 CONDOS","11865 - OUT OF BOUNDS","11875 - THE OAKS","11885 - BAY ROCK","11886 - PALM WEST AT APPLEHEAD CASITAS","11887 - PALM VILLAS CONDOS","11900 - PARKRIDGE GREEN","11925 - PORT OF CALL","11950 - PORTOFINA","11975 - PRIMA CASA","11976 - THE CLIFFS AT WATER RIDGE","11977 - THE CAPE","11978 - THE QUARTERS","11979 - THE ENCLAVE AT HORSESHOE BAY","11985 - THE COMMONS AT LAKESHORE","12000 - SANDPIPER LANDING","12025 - THE SANDTRAP","12030 - SERENITY COVE","12050 - SHELTERED HARBOR","12075 - SHORELINE","12080 - SLICK ROCK","12123 - STONEGATE","12150 - STONES THROW","12160 - STONEWOOD","12200 - THE SUNDOWNER","12225 - TALL TIMBERS","12250 - TIMBER RIDGE","12258 - TRAILS POINT","12259 - TRAILS END","12260 - TRANQUILO BAY","12275 - THE UPPER DECK","12300 - UPPER DECK 2","12320 - VERDE VISTA CONDO","12325 - VILLAS ALBANAS","12330 - VILLAS DE LAGO","12350 - WATER RIDGE SUBDIVISION","12400 - WESTERN TRACE","12410 - WESTRIDGE","12425 - WILD ROSE VILLA","12450 - WINDGATE","12460 - WINDJAMMER","12475 - WINDSONG","12500 - THE HILLS","12505 - THE HILLS OF AZURITE CONDOMINIUMS","12510 - INDIAN BEND","12526 - INDIAN HILLS","12535 - INDIAN SPRINGS","12545 - INKS LAKE","12555 - ISLAND LODGES","12565 - ISLAND VILLAGE","12570 - ISLAND VILLAGE WEST","12575 - JECKERS COVE","12577 - JODYS LANDING","12579 - KINGDOM ESTATES","12580 - KINGFREE","12585 - KINGSLAND","12605 - KINGSLAND ESTATES","12615 - KINGSLAND INDUSTRIAL","12616 - KINGSLAND TERRACE CONDO","12618 - KINGSLAND WEST ESTATES","12630 - KINGSRIDGE","12635 - KINGSWAY SUBD","12637 - LAGO ESCONDIDO","12640 - LA JOLLA","12650 - LAKESHORE CENTER","12655 - LAKE SHORE RANCH","12660 - LAKESIDE HEIGHTS","12670 - LAKEVIEW ACRES","12680 - LAKEWOOD FOREST I II  III","12685 - LAZY OAKS","12686 - LEGEND HILLS","12688 - LEHNE RANCH","12690 - LEONALAND","12695 - LIGHTHOUSE COUNTRY CLUB","12700 - LITTLE MIDLAND","12701 - LLANO CROSSING","12705 - LLANO OAKS","12710 - LOMA LINDA","12720 - LONG MOUNTAIN ESTATES","12730 - LULA HAYWOOD","12735 - MARLYN POINT","12740 - BILLY MCGEE","12750 - JOHN MILLER","12752 - MITCHELL POINT AT SKYWATER","12760 - EV MITCHELL","12770 - MOUNTAIN VIEW RANCH","12780 - MURRAY NORED","12790 - MURCHISON","12795 - MURCHISON HILL SUBD","12800 - IKE MURCHISON","12810 - MA MURCHISON","12815 - MARBLE SPRINGS RANCH","12820 - MURCHISON RESUB","12825 - NANOAN TRACT","12830 - NOB HILL","12835 - NORTH RIDGE ACRES","12840 - OAK POINT","12845 - OO RANCHETTES","12850 - OAK RIDGE ADDITION","12860 - OAK RIDGE ESTATES","12870 - OTTS ADDITION","12872 - OVERSTREET RIVER ACRES SUBD","12875 - PACKSADDLE GREEN","12877 - PACKSADDLE VIEW","12880 - PARADISE POINT","12890 - PEBBLE BEACH","12895 - PECAN CREEK","12900 - PIERCE POINT","12910 - PIONEER","12915 - POINT BREEZE","12920 - POINT TELLA","12925 - POINTVIEW LAKE HOMES","12928 - POST OAK PARK","12930 - FC PORTER","12940 - QUARRY","12950 - QUAIL CREEK","12960 - QUAIL HOLLOW","12970 - QUAIL RIDGE 20505","12980 - REVEN ACRES","12990 - RIO LLANO","13000 - RIO VISTA","13005 - RIVER BEND","13007 - RIVER COTTAGE","13010 - RIVER HILLS","13015 - RIVERLAKE","13020 - RIVER VALLEY","13030 - RIVERSIDE","13040 - RIVER VIEW","13042 - ROCKY HILLS","13045 - ROCKY MOUNTAIN RANCH","13050 - ROSE HILL","13055 - ROUGH HOLLOW RANCH","13060 - ROYAL OAKS ESTATES 1-4","13070 - ROYAL OAKS ESTATES 5","13071 - ROYAL OAKS ESTATES 6","13072 - ROYAL OAKS ESTATES 7","13073 - ROYAL OAKS ESTATES 8","13074 - ROYAL OAKS ESTATES 9","13075 - ROYAL OAKS ESTATES 10","13076 - ROYAL OAKS ESTATES 11","13080 - ROYAL OAKS COUNTRY CLUB","13090 - ROYAL OAKS RANCHETTES","13100 - SANDY HARBOR","13105 - SANDY MOUNTAIN ESTATES","13108 - SCHEERBAUM PLATZ SUBDIVISION","13110 - SCOTT ACRES","13112 - SENDERA RIDGE ABST 859","13115 - SHIRLEY WILLIAMS","13120 - SHIRWOOD ACRES","13121 - SHOOTING STAR","13122 - SIENA CREEK PHASE ONE","13125 - SKYLINE DRIVE","13127 - SUMMIT ROCK","13130 - SOUTH COVE","13131 - STRIPER POINT","13132 - SUMMERPLACE","13133 - SUNRISE BEACH AG","13134 - SUNRISE BEACH MOUNTAIN TOP","13136 - SUMMIT ROCK PRO SHOP SALES CENTER  VILLAS","13137 - SUMMIT ROCK GOLF COURSE","13140 - SUNRISE BEACH","13150 - TERRACE PARK","13151 - THE GROVE AT SUMMIT ROCK","13154 - THE RESERVE AT SUMMIT ROCK","13155 - THREADGILL","13156 - THREE SPRINGS RANCH","13157 - THUNDERBIRD AIRPARK","13160 - TIMBERCOVE","13170 - TIMBER LAKE","13180 - TOW","13190 - TOW VILLAGE","13200 - WC TREADWILL","13210 - TUMLINSON","13215 - VALLEY RANCH","13220 - VALLEY SPRING","13230 - VAN ACRES","13231 - VILLAS AT SIENA CREEK CONDO","13233 - VISTA ENCANTADA","13234 - THE TRAILS OF LAKE LBJ","13235 - THE VILLAGE KINGSLAND","13237 - THE ANTLERS ON LAKE LBJ","13238 - THE VILLAGE","13239 - THE WATERS AT HORSESHOE BAY RESORT CONDOMINIUM","13240 - WADE GARDEN 20595","13250 - WARWICK ESTATES","13252 - WATERS EDGE AT SUNRISE BEACH VILLAGE","13253 - WESTGATE AT HORSESHOE BAY","13255 - WILD OAK ACRES","13258 - WIND DANCER","13259 - WOLF CREEK ESTATES","13260 - WOODS EDGE","13270 - WOOD FOREST RANCHETTES","13280 - BARLER","13290 - BROWN","13300 - CENTRAL PARK","13310 - DAVIS","13320 - FAIRVIEW","13330 - HOLDEN","13340 - LETCHER","13350 - L I  F","13360 - MARSCHALL","13365 - MC LEAN","13370 - MILLER","13380 - MOORE","13390 - MOUNTAIN VIEW ESTATES","13400 - NORTH","13410 - OATMAN","13430 - OZBURN","13440 - PARKVIEW ACRES","13450 - SOUTH","13460 - STITH","13470 - WOOLEY","13480 - WOOTAN","14760 - FIFTY FIVE AND FINER MH PARK","83630 - PERSONAL PROPERTY","84000 - IMPROVEMENT ONLY","88170 - PERSONAL PROPERTY - UTILITY"]
var processSearchResults = function(search, subdivision, page, previousItemCount ) {
    page = typeof page !== 'undefined' ? page : 1;
    previousItemCount = typeof previousItemCount !== 'undefined' ? previousItemCount : 0;
    subdivision = typeof subdivision !== 'undefined' ? subdivision : '';

    $.ajax({
        type: 'GET',
        url: 'keyword-request.php',
        data: {
            keyword: encodeURIComponent(search),
            page: page
        },
        dataType: 'json',
        success: function(json) {
            //console.log($(html));
            //var testreponse = $('<div/>').html(html).contents();
            var results = json.ResultsList;
            var totalPages = Number(json.TotalPages);
            var totalResults = Number(json.TotalResults);
            var currentPage = Number(json.Page);
            var percentCompleted;
            $('#total-results').text(totalResults);
            console.log('current page: ' + currentPage);

            var itemsProcessed = 0;
            var numItems = results.length;

            function looper(itemsProcessed){
                if (window.reset === 'reset') {
                    window.reset = null;
                } else {
                    setTimeout(function(){
                        var grandTotal = itemsProcessed + previousItemCount + 1;
                        console.log('grand total:'+ grandTotal);
                        var resultsRemaining = parseInt(totalResults) - grandTotal;
                        var minutesToProcess = Math.round((parseInt(resultsRemaining) * 2) / 60); //49 * 2 / 60 = 2

                        var item = results[itemsProcessed];
                        var id = item.PropertyId;
                        $('#results').text('Processing property:' + id);

                        if (grandTotal == totalResults) {
                            percentCompleted = 100;
                            setTimeout(function(){
                                $('#results').text('All done!');
                            }, 1000);
                        } else {
                            percentCompleted = Math.floor((grandTotal / totalResults) * 100); //(1 / 50) * 100 = 2%
                        }
                        $('#results-remaining').text(resultsRemaining);
                        $('#time-remaining').text(minutesToProcess);
                        $('#percent-completed').width(percentCompleted + '%');
                        $('#percentage-text').text(percentCompleted + '%');
                        console.log(id);
                        var propertyUrl = 'http://esearch.llanocad.net/Property/View/' + id;
                        processProperty(propertyUrl, subdivision);
                        itemsProcessed++;
                        //console.log(currentPage + ', ' + itemsProcessed);
                        if( itemsProcessed < numItems) {
                            console.log('next item');
                            looper(itemsProcessed);
                        } else {
                            if (currentPage < totalPages) {
                                console.log('increment and start over on the next page');
                                console.log('new current page: ' + currentPage);
                                var previousItems = itemsProcessed + previousItemCount;
                                currentPage++;

                                processSearchResults(search, subdivision, currentPage, previousItems);
                            }
                        }
                    }, 2000);
                }
            }
            //starts at 0
            looper(itemsProcessed);

        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log(textStatus);
        }
    });
}
var processProperty = function(propertyUrl, subdivision) {

    if (propertyUrl.match("/Property/View/")) {
        var property = {};

        console.log('processing ' + propertyUrl);
        $.ajax({
            type: 'GET',
            url: 'property-request.php',
            dataType: 'html',
            data: {
                url: propertyUrl
            },
            success: function(html){

                //account
                property.id = $(html).find('th:contains("Property ID:")').siblings('td').text();
                property.legalDescription = $(html).find('th:contains("Legal Description:")').siblings('td').text();
                property.geoID = $(html).find('th:contains("Geographic ID:")').siblings('td').text();
                property.agentCode = $(html).find('th:contains("Agent Code:")').siblings('td').text();
                property.type = $(html).find('th:contains("Type::")').siblings('td').text();

                //location
                $(html).find('th:contains("Address:")').each(function(){
                    //differentiate between "Mailing Address:", which contains the same string
                    if ($(this).closest('tr').prev().hasClass('form-table-title')){
                        property.address = $(this).siblings('td').innerText();
                    }
                });
                property.mapID = $(html).find('th:contains("Map ID:")').siblings('td').text();
                property.neighborhoodCD = $(html).find('th:contains("Neighborhood CD:")').siblings('td').text();
                property.subdivision = subdivision;
                //owner
                property.ownerID = $(html).find('th:contains("Owner ID:")').siblings('td').text();
                property.ownerName = $(html).find('th:contains("Name:")').siblings('td').text();
                property.mailingAddress = $(html).find('th:contains("Mailing Address:")').siblings('td').innerText();
                //console.log(property.mailingAddress);
                property.percentOwnership = $(html).find('th:contains("% Ownership:")').siblings('td').text();
                property.exemptions = $(html).find('th:contains("Exemptions:")').siblings('td').text();


                //property type
                property.land = $(html).find('h3:contains("Property Land")').closest('.panel-heading').siblings('.table-responsive').find('tr:nth-child(2) td:first-child').text().slice(0,2);

                //console.log(property.address);
                //property values
                //property.improvementHomesiteValue
                //property.improvementNonHomesiteValue
                //property.landHomesiteValue
                //property.agriculturalMarketValuation
                //property.marketValue
                //property.agUseValue
                //property.appraisedValue
                //property.hsCap
                //property.assessedValue


                //console.log(property.id);
            },
            complete: function() {
                $.ajax({
                    type: 'GET',
                    url: 'process-property.php',
                    data: {
                        id: encodeURIComponent(property.id),
                        legalDescription: encodeURIComponent(property.legalDescription),
                        geoID: encodeURIComponent(property.geoID),
                        agentCode: encodeURIComponent(property.agentCode),
                        type: encodeURIComponent(property.type),
                        address: encodeURIComponent(property.address.replace(/(\r\n|\n|\r)/gm,"\n")),
                        mapID: encodeURIComponent(property.mapID),
                        neighborhoodCD: encodeURIComponent(property.neighborhoodCD),
                        ownerID: encodeURIComponent(property.ownerID),
                        ownerName: encodeURIComponent(property.ownerName),
                        mailingAddress: encodeURIComponent(property.mailingAddress.replace(/(\r\n|\n|\r)/gm,"\n")),
                        percentOwnership: encodeURIComponent(property.percentOwnership),
                        exemptions: encodeURIComponent(property.exemptions.replace(/(\r\n|\n|\r)/gm,"\n")),
                        land: encodeURIComponent(property.land),
                        subdivision: encodeURIComponent(property.subdivision)
                    },
                    dataType: 'html',
                    success: function() {
                        if (property.type == 'A' || property.land)
                        console.log('success!');
                    }
                });
            }
        });
    }
}

function UpdateQueryString(key, value, url) {
    console.log('updating');
    if (!url) url = window.location.href;
    var re = new RegExp("([?&])" + key + "=.*?(&|#|$)(.*)", "gi"),
        hash;

    if (re.test(url)) {
        if (typeof value !== 'undefined' && value !== null)
            window.location.href = url.replace(re, '$1' + key + "=" + value + '$2$3');
        else {
            hash = url.split('#');
            url = hash[0].replace(re, '$1$3').replace(/(&|\?)$/, '');
            if (typeof hash[1] !== 'undefined' && hash[1] !== null)
                url += '#' + hash[1];
            console.log(url);
            window.location.href = url;
        }
    }
    else {
        if (typeof value !== 'undefined' && value !== null) {
            var separator = url.indexOf('?') !== -1 ? '&' : '?';
            hash = url.split('#');
            url = hash[0] + separator + key + '=' + value;
            if (typeof hash[1] !== 'undefined' && hash[1] !== null)
                url += '#' + hash[1];
            console.log(url);
            window.location.href = url;
        }
        else
            console.log(url);
            window.location.href = url;
    }
}

$(document).ready(function(){
    for (var i = 0, len = subdivisions.length; i < len; i++) {
        var item = subdivisions[i];
        $('#subdivisions-dropdown').append('<option value="' + item + '">' + item + '</option>');
    }

    $('#cad-form').submit(function(e){
        e.preventDefault();

        var text;
        if ( $('#search').val() !== '' ) {
            text = $('#search').val();
        } else {
            text = $('#subdivisions-dropdown').val();
        }

        var searchType = $('input[name="search-type"]:checked').val();
        //console.log(url);
        if ( searchType === 'subdivision') {
            var subID = text;

            text = 'Subdivision:"'+ text +'"';
            processSearchResults(text, subID);
        } else {
            processSearchResults(text);
        }
        $('#cad-form').hide();
        $('#waiting-text').show();
    });

    $('input[name="search-type"]').change(function(){
       $('#subdivisions-dropdown').toggle();
        $('.sub-or').toggle();
    });
    $('td.property-update form input').change(function(event){
        //console.log('test');
        //$(this).prop("checked", !$(this).prop("checked"))
        var key;
        if ($(this).hasClass('property-mailed')) {
            key = 'mailed';
        } else if ($(this).hasClass('property-wont-use')) {
            key = 'wontUse';
        }
        var value = $(this).prop('checked') ? 1 : 0;
        //var mailed = $('.property-mailed',this).val();
        //var wontUse = $('.property-wont-use',this).val();
        var propertyId = $(this).closest('td').siblings('.property_id').text();
        var url = window.location.href;

        console.log('key ' + key);
        console.log('value ' + value);
        $('body').addClass('loading');
        $.ajax({
                type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
                url         : 'property-table.php', // the url where we want to POST
                data        : {
                    key: key,
                    value: value,
                    propertyID: propertyId
                }, // our data object
                dataType    : 'text', // what type of data do we expect back from the server
            })
            // using the done promise callback
            .success(function(data) {

                // log data to the console so we can see
                console.log(data);

                // here we will handle errors and validation messages
            })
            .error(function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus);
                console.log(errorThrown)
            })
            .complete(function(){
                $('body').removeClass('loading');
                window.location.reload(true);
            });

        // stop the form from submitting the normal way and refreshing the page
        //event.preventDefault();
    });
    $('#reset').click(function(e){
        e.preventDefault();
        window.reset = 'reset';
        $('#search').val('');
        $('#subdivisions-dropdown').val('none');
        $('#cad-form').show();
        $('#waiting-text').hide();
        $('#results').text('');
    });

    $('.property-update').each(function(){
        var id = $(this).siblings('.property_id').text();
        //console.log(id);
        $(this).find('input[type=hidden]').attr('value',id);
    });
});

//make innerText function preserve whitespace
(function($){
    $.fn.innerText = function(msg) {
        if (msg) {
            if (document.body.innerText) {
                for (var i in this) {
                    this[i].innerText = msg;
                }
            } else {
                for (var i in this) {
                    this[i].innerHTML.replace(/&amp;lt;br&amp;gt;/gi,"n").replace(/(&amp;lt;([^&amp;gt;]+)&amp;gt;)/gi, "");
                }
            }
            return this;
        } else {
            if (document.body.innerText) {
                return this[0].innerText;
            } else {
                return this[0].innerHTML.replace(/&amp;lt;br&amp;gt;/gi,"n").replace(/(&amp;lt;([^&amp;gt;]+)&amp;gt;)/gi, "");
            }
        }
    };
})(jQuery);