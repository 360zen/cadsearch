<?php
/**
 * Created by PhpStorm.
 * User: justin
 * Date: 2/12/16
 * Time: 1:58 PM
 */

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title><?php echo $page_title; ?></title>
    <link rel="stylesheet" href="css/main.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <script src="main.js"></script>
</head>
<body>
<div id="overlay"></div>
<h1><?php echo $page_title; ?></h1>
<?php
$searchClass = $page_title == 'Property Search' ? 'current' : '';
$leadsClass = $page_title == 'Hot Leads' ? 'current' : '';
?>
<ul id="main-menu" class="menu">
    <li class="<?php echo $searchClass; ?>"><a href="index.php">Search</a></li>
    <li class="<?php echo $leadsClass; ?>"><a href="leads.php">Leads</a></li>
</ul>
