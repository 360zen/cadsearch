<?php
/**
 * Created by PhpStorm.
 * User: justin
 * Date: 2/12/16
 * Time: 2:19 PM
 */
require_once 'db-config.php';
$key = isset($_POST['key']) ? $_POST['key'] : null;
$value = isset($_POST['value']) ? $_POST['value'] : null;
$id = $_POST['propertyID'];

try {
    if ($key === 'mailed') {
        $stmt = $db->prepare("UPDATE properties SET mailed=:mailed WHERE property_id=:property_id");

        $stmt->execute( array(
            'mailed' => $value,
            'property_id' => $id
        ));
        if (!$stmt) {
            echo "\nPDO::errorInfo():\n";
            print_r($db->errorInfo());
        } else {
            echo 'it worked. mailed changed.';
        }
    } elseif ($key === 'wontUse') {
        $stmt = $db->prepare("UPDATE properties SET will_not_use=:wont_use WHERE property_id=:property_id");

        $stmt->execute( array(
            'wont_use' => $value,
            'property_id' => $id
        ));
        if (!$stmt) {
            echo "\nPDO::errorInfo():\n";
            print_r($db->errorInfo());
        } else {
            echo 'it worked. wontUse changed.';
        }
    }


//    header('Location:'. $_SERVER["HTTP_REFERER"]);
} catch(PDOException $ex) {
    echo 'Error connecting to the database';
    echo $ex->getMessage();
}

