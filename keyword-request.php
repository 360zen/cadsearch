<?php
/**
 * Created by PhpStorm.
 * User: justin
 * Date: 2/11/16
 * Time: 10:28 PM
 */

//keyword search
$propertyUrl = 'http://esearch.llanocad.net/Search/SearchResults/?keywords=' . $_GET['keyword'] . '&filter=&page='. $_GET['page'] . '&pageSize=25&skip=0&take=25';


// create curl resource
$ch = curl_init();

// set url
curl_setopt($ch, CURLOPT_URL, $propertyUrl);

curl_setopt($ch, CURLOPT_HTTPHEADER, array('Cookie: grid-view=PropertyId%2CGeoId%2CPropertyType%2COwnerName%2CAddress%2CAppraisedValueDisplay%2C; page-size=25'));
//return the transfer as a string
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

// $output contains the output string
$output = curl_exec($ch);

// close curl resource to free up system resources
curl_close($ch);

echo $output;