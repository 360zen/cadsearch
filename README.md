This project is a small app that crawls the Llano Cad Search site for properties with specific criteria and adds them to a database.

To use, visit the index file and use the search form. Then visit /leads.php to see the table you've generated.

You will need a `db-config.php` file with a $db variable equal to a new PDO() to create the database.